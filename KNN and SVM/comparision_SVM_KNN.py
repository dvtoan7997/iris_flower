from sklearn import datasets		
from sklearn import svm    			
import numpy as np
import matplotlib.pyplot as plt         
from sklearn import neighbors, datasets
   
iris = datasets.load_iris()
iris_X = iris.data
iris_y = iris.target
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(
     iris_X, iris_y, test_size=20)
from sklearn.svm import SVC

#Linear Kennel SVC
clf = SVC(kernel = 'linear', C = 1e5) # just a big number 

clf.fit(X_train, y_train) 

w = clf.coef_
b = clf.intercept_
print('w = ', w)
print('b = ', b)


y_predict = clf.predict(X_test)
#print('y_predict=', y_predict)
#print('y_test=', y_test)
from sklearn.metrics import accuracy_score
print ("Accuracy of Kernel_linear SVC: %.2f %%" %(100*accuracy_score(y_test, y_predict)))

#KNN
clf1 = neighbors.KNeighborsClassifier(n_neighbors = 3, p = 2)
clf1.fit(X_train, y_train)
y_pred = clf.predict(X_test)
#print(y_pred)
print("Accuracy of KNN: %.2f %%" %(100*accuracy_score(y_test, y_pred)))


clf2 = SVC(kernel='poly', degree = 3, gamma=1, C = 1e5)
clf2.fit(X_train, y_train)
y_pred2 = clf.predict(X_test)
print("Accuracy of Kernel_poly: %.2f %%" %(100*accuracy_score(y_test, y_pred2)))